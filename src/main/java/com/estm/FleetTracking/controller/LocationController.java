package com.estm.FleetTracking.controller;

import com.estm.FleetTracking.exception.ResourceNotFoundException;
import com.estm.FleetTracking.model.Location;
import com.estm.FleetTracking.model.User;
import com.estm.FleetTracking.repository.LocationRepository;
import com.estm.FleetTracking.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8000", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class LocationController {

  @Autowired
  private LocationRepository locationRepository;
  private UserRepository userRepository;
  
  @GetMapping("/locations")
  public List<Location> getAllLocations() {
    return locationRepository.findAll();
  }
  
  @GetMapping("/locations/users/{id}")
  public List<Location> getLocationByUserId(@PathVariable(value = "id") Long userId)
      throws ResourceNotFoundException {
   
	  List<Location> Listall = getAllLocations();
	  List<Location> l = new ArrayList<Location>();
	  
	  for (Location el:Listall) {
		  if (el.getUser().getId() == userId) l.add(el);
	  }
	  
    return l;
  }
  
  @GetMapping("/locations/{id}")
  public ResponseEntity<Location> getLocationById(@PathVariable(value = "id") Long locationId)
      throws ResourceNotFoundException {
    Location location =
    	locationRepository
            .findById(locationId)
            .orElseThrow(() -> new ResourceNotFoundException("Location not found on :: " + locationId));
    User user =
            userRepository
                .findById(location.getUser().getId())
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + location.getUser().getId()));
    
    location.setUser(user);
    return ResponseEntity.ok().body(location);
  }

  
  @PostMapping("/locations")
  public Location createLocation(@Valid @RequestBody Location location) {
    return locationRepository.save(location);
  }


  @DeleteMapping("/locations/{id}")
  public Map<String, Boolean> deleteLocation(@PathVariable(value = "id") Long locationId) throws Exception {
    Location location =
    	locationRepository
            .findById(locationId)
            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + locationId));

    locationRepository.delete(location);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
  
  @DeleteMapping("/locations/user/{id}")
  public Map<String, Boolean> deleteuserLocation(@PathVariable(value = "id") Long UserId) throws Exception {
	List<Location> list = getLocationByUserId(UserId);
	for(Location l:list) {
		locationRepository.delete(l);
	}
	
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
  
}
