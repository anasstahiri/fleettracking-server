package com.estm.FleetTracking.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "locations")
@EntityListeners(AuditingEntityListener.class)
public class Location{
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_location;
	
    @Column(name = "Latitude", nullable = false)
    private double Latitude;

    @Column(name = "Longitude", nullable = false)
    private double Longitude;
    
    @Column(name = "Speed", nullable = false)
    private double Speed;

	@Column(name = "datetime")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date datetime;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", nullable = false)
    private User user;

    
    
	public long getId() {
		return id_location;
	}


	public void setId(long id) {
		this.id_location = id;
	}
	
    public double getSpeed() {
		return Speed;
	}


	public void setSpeed(double speed) {
		Speed = speed;
	}

	public double getLatitude() {
		return Latitude;
	}


	public void setLatitude(double latitude) {
		Latitude = latitude;
	}


	public double getLongitude() {
		return Longitude;
	}


	public void setLongitude(double longitude) {
		Longitude = longitude;
	}


	public Date getDatetime() {
		return datetime;
	}


	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

    
    @Override
    public String toString() {
        return "Location{" +
                "id=" + id_location +
                ", Latitude='" + Latitude + '\'' +
                ", Longitude='" + Longitude + '\'' +
                ", Latitude='" + Latitude + '\'' +
                ", Longitude='" + datetime+ '\'' +
                ", User='" + user + '\'' +
                '}';
    }
    
}
